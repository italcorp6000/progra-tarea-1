﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tareosa
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Estudiante> estudiantes = new List<Estudiante>
        {
            new Estudiante("Mariano", "Brando", 1, new List<double> { 15, 10, 18 }),
            new Estudiante("Enlace", "López", 2, new List<double> { 12, 18, 16 })
        };

            foreach (var estudiante in estudiantes)
            {
                estudiante.CalcularPromedio();
            }
        }
    }
}
