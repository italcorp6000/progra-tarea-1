﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tareosa
{
    public class Estudiante : Persona, IEstudiante
    {
        public int Matricula { get; set; }
        public List<double> Notas { get; set; }
        public Estudiante(string nombre, string apellido, int matricula, List<double> notas)
        {
            Nombre = nombre;
            Apellido = apellido;
            Matricula = matricula;
            Notas = notas;
        }
        public void CalcularPromedio()
        {
            double suma = 0; 
            foreach (var nota in Notas)
            {
                suma += nota; 
            }
            double promedio = suma / Notas.Count; 
        }
    }
}
